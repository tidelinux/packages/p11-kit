# This is the source.sh script. It is executed by BPM in a temporary directory when compiling a source package
# BPM Expects the source code to be extracted into the automatically created 'source' directory which can be accessed using $BPM_SOURCE
# BPM Expects the output files to be present in the automatically created 'output' directory which can be accessed using $BPM_OUTPUT

DOWNLOAD="https://github.com/p11-glue/p11-kit/releases/download/${BPM_PKG_VERSION}/p11-kit-${BPM_PKG_VERSION}.tar.xz"
FILENAME="${DOWNLOAD##*/}"

# The prepare function is executed in the root of the temp directory
# This function is used for downloading files and putting them into the correct location
prepare() {
  wget "$DOWNLOAD"
  tar -xvf "$FILENAME" --strip-components=1 -C "$BPM_SOURCE"
}

# The build function is executed in the source directory
# This function is used to compile the source code
build() {
  mkdir p11-build
  cd p11-build
  meson setup ..            \
        --prefix=/usr       \
        --libdir=/usr/lib   \
        --buildtype=release \
        -Dtrust_paths=/etc/pki/anchors
  ninja
}

# The check function is executed in the source directory
# This function is used to run tests to verify the package has been compiled correctly
check() {
  cd p11-build
  LC=ALL=C ninja test
}

# The package function is executed in the source directory
# This function is used to move the compiled files into the output directory
package() {
  cd p11-build
  DESTDIR="$BPM_OUTPUT" ninja install   
  ln -sfv /usr/libexec/p11-kit/trust-extract-compat "$BPM_OUTPUT"/usr/bin/update-ca-certificates
}
